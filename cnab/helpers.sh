#!/usr/bin/env bash
set -euo pipefail

todo() {
  echo "TODO"
}

prepare-manifest() {
  export IPS_NAME=$1
  auth=$(echo -n "$USERNAME:$PASSWORD" | base64 -w 0)
  export IPS_JSON=$(echo -n "{\"auths\":{\"$REGISTRY\":{\"username\":\"$USERNAME\",\"password\":\"$PASSWORD\",\"email\":\"$EMAIL\",\"auth\":\"$auth\"}}}")
  gomplate -f k8s/ips-template.yaml -o k8s/ips.yaml
  cat k8s/ips.yaml
}

delete-secret() {
  ipsname=$1
  kubectl delete secret -n "$NAMESPACE" $ipsname
}

# Call the requested function and pass the arguments as-is
"$@"
